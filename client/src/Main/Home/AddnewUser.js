import React from 'react';
import  {Component}  from 'react';
//import '../Login/Login.css';
//import 'tachyons';
import './AddnewUser.css'

import { Redirect } from 'react-router-dom';
import { BrowserRouter , Route, Link, Switch } from 'react-router-dom';


class AddnewUser extends React.Component {
  constructor(props) {

    super(props);
    this.state = {
    	error:'',
      id:'',
    	username:'',
    	password:'',
    	email:'',
    	confirm_password:'',
      mobile_number:''
    	};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit(event) {
    
    event.preventDefault();
    const requestOptions = {
        method: 'POST',
        headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
         },
        body: JSON.stringify({ username: this.state.username,email:this.state.email,password:this.state.password,confirm_password:this.state.confirm_password,mobile_number:this.state.mobile_number})
    }
     fetch('/api/v1/users', requestOptions)
        .then(response=>{
        	if(response.ok){
                this.props.history.push('/signin')
              }
              else{
                this.setState({
                  error:'some thing went wrong',
                  id:'',
                  username:'',
                  password:'',
                  email:'',
                  confirm_password:'',
                  mobile_number:''
                })
		         }
        })
   }

componentDidMount(){
  if(this.props.match.params.id){
    var id = this.props.match.params.id;
    fetch('/user/'+id).then(response=>response.json()).then(data=>{
    
      this.setState({
        id:id,
        username:data.username,
        password:data.password,
        email:data.email,
        confirm_password:data.confirm_password,
        mobile_number:data.mobile_number

        })
     })
   }
 }

update=(event)=>{
      event.preventDefault();
      var id = this.state.id
      const requestOptions = {
        method: 'PUT',
        headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        },
      body:JSON.stringify({
          username:this.state.username,
          password:this.state.password,
          email:this.state.email,
          confirm_password:this.state.confirm_password,
          mobile_number:this.state.mobile_number
        })
       };
     

  fetch("/api/v1/users/"+id,requestOptions).then(response=>{
    if(response.ok){
                this.props.history.push('/')
              }
              else{
                this.setState({
                  error:'some thing went wrong',
                  id:this.state.id,
                  username:this.state.username,
                  password:this.state.password,
                  email:this.state.email,
                  confirm_password:this.state.confirm_password,
                  mobile_number:this.state.mobile_number
                })
             }

  })

}

  render() {
    
    if(this.props.match.params.id){
     
      return(
        <div class="left-box">
          
          <div>{this.state.error}</div>

          <form onSubmit={this.update}>
            
              <input type="text" name="username" value={this.state.username} placeholder="name" onChange={this.handleChange} required/>
             
              <input type="email" name="email" placeholder="email" value={this.state.email} onChange={this.handleChange}  required/>
              
              <input type="tel" name="mobile_number"  placeholder="enter mobile_number" value={this.state.mobile_number} onChange={this.handleChange} pattern="[6-9]{1}[0-9]{9}"  required />
          
            <input type="submit" value="Submit" />
          </form>
        </div>

        )
      }
    else{
      console.log('inside else',this.state.id)

        return (
              <div class="left-box">
               
                  <div><h1>Create New User</h1></div>
                

                  <div>{this.state.error}</div>



                <form onSubmit={this.handleSubmit}>
                   <input type="text" name="username" value={this.state.username} placeholder="name" onChange={this.handleChange} required/><br></br>
                    
                    <input type="password" name="password" placeholder="password" value={this.state.password} onChange={this.handleChange} pattern=".{8,20}" required/><br></br>
                    
                    <input type="email" name="email" placeholder="email" value={this.state.email} onChange={this.handleChange}  required/><br></br>
                   
                    <input type="tel" name="mobile_number"  placeholder="enter mobile_number" value={this.state.mobile_number} onChange={this.handleChange} pattern="[6-9]{1}[0-9]{9}" required />
                  
                  <input type="submit" value="Submit" />
                </form>
              </div>

            );
        }
      }
  }

export default AddnewUser;