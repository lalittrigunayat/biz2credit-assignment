const mongoose=require('mongoose')
mongooseTypePhone = require('mongoose-type-phone');
const User=mongoose.Schema({
	_id:{
		type:Number,
		default:1
	},
	username:{
		type:String,
		require:true
	},
	email:{ 
		type: String, 
		lowercase: true, 
		require: true 
	},
	password:{
		type:String,
		require:true
	},
	
	mobile_number:{
		type:Number,
		require: true
	},
	created_by:{
		type:Number,
		require:true

	}
	
})

const Login=mongoose.Schema({
	username:{
		type:String,
		required:true
	},
	password:{
		type:String,
		required:true
	}

})

module.exports=mongoose.model('login_Model',Login)
module.exports=mongoose.model('User',User)