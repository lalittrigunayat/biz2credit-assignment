import React from 'react';

import '../Common.css'
import {Redirect, BrowserRouter, Route, Link, Switch } from 'react-router-dom';
import Register from '../Register/Registration.js';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state={
          username:'',
          password:'',
          isLogin:false
        }
       
    }


componentDidMount(){
  fetch('/get_session').then(response=>{

    if(response.ok){
      this.setState({
          isLogin:true
        })
      }
    
  })
}


 handleChange=(event)=> {
  
    this.setState({[event.target.name]: event.target.value});
  }
    
submitform = (e) => {
      e.preventDefault()
      let username=e.target[0].value
      let password=e.target[1].value
      
     
        //logic magic
        const requestOptions = {
            method: 'POST',

            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({ username: username, password: password })
        };
        fetch('/api/v1/session', requestOptions)
            .then(response  => {
                 if(response.ok){
                  console.log('inside ok')
                  var token = localStorage.setItem("username",username)
                  this.setState({
                    isLogin:true
                  })
                    
                }

                })
              }

    render() {
        
        if (this.state.isLogin){

          return <Redirect to='/' />
        }
        else{

      return (
            <div class="login-form">

              <h1>Login Form</h1>
              <form onSubmit={this.submitform}>
                <input type="text" name="username"  placeholder="Username" value={this.state.username} onChange={this.handleChange} required />
                <input type="password" name="password" placeholder="password" value={this.state.password}  onChange={this.handleChange} required/>
                <input type="submit" />
              
              </form>
        
            <Link to="/signup">register</Link>
              
          </div>


    )

        }
        

  }
}

export default Login;